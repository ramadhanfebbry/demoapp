class CpPaymentsController < ApplicationController
	def index
		response = RestClient::Request.new(
          :method => :post,
          :url => params[:url],
          :payload =>  params[:body].to_json,
          :user => params[:basic_username],
          :password => params[:basic_password],
          :headers => params[:headers]
      ).execute
    rescue RestClient::BadRequest => err
      puts err.response.body
      puts "BAD REQUEST ERROR FOR CP table service"
      response = err.response.body
    rescue => e
      p e.class
      p e.inspect.class
      if e.inspect.include?('401 Unauthorized: ')
        response = e.inspect.gsub('401 Unauthorized: ','')
      end
      render json: response.to_json
	end
end
