class ApplicationController < ActionController::API
  include ActionController::RequestForgeryProtection
  include ActionController::HttpAuthentication::Basic::ControllerMethods 
  http_basic_authenticate_with name: "dhh", password: "secret"
  before_action :set_params, only: [:index]

  def set_params
  	params.merge!(request.headers)
  end
end
