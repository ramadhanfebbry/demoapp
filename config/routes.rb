Rails.application.routes.draw do
  root 'home#index'

	scope '/api' do
	  resources :cp_payments
	end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
