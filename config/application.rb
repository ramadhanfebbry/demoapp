require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module DemoApp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
		config.api_only = true
		config.generators do |g|
		  g.helper false
		  g.assets false
		  g.template_engine = false		  
		  g.view_specs false
		  g.helper_specs false		  
		  g.tests false
		end

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

		config.debug_exception_response_format = :api

  end
end
